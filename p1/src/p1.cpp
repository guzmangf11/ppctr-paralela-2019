#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <thread>
#include <mutex>
#include <unistd.h>
#include <condition_variable>
#include <sys/time.h>

//#define FEATURE_LOGGER 
//#define DEBUG
#define FEATURE_OPTIMIZE 1

#if FEATURE_OPTIMIZE
	#include <atomic>
	std::atomic<double> resultadoAtomico(0);
#endif

//Mutex para controlar el acceso a las variables
std::mutex g_m;
//Booleano que indica si el hilo tiene que hacer el resto de lo que queda del bucle
bool haceResto=true;
//Numero de peticiones realizadas por los hilos
int peticiones=0;

//Para medir el tiempo de ejecucion
struct timeval tiempoInicio,tiempoFin;

#ifdef FEATURE_LOGGER
std::condition_variable g_c;
bool g_datoALogger=false;
//Variable global que almacena el resultado del logger
double resultadoLogger;
#endif

//El primer argumento indica la operacion y se realiza la operacion en los argumentos 2 y 3
double operar(int operacion,double resultado, double siguiente){
	#ifdef DEBUG
	printf("Dentro metodo: operacion %d resultado %f siguiente %f\n",operacion,*resultado,siguiente);
	#endif 
	switch(operacion){
		case 0:
			return (int)(resultado)^(int)siguiente;
		case 1:
			return resultado+siguiente;
		case 2:
			return resultado-siguiente;
	}
}


void reduccion(double *elementos, double *resultado, int operacion, int numElementos, int numHilos, int indiceHilo, double *arrayResultadosParciales){
	double resultadoHilo=0;
	#ifdef DEBUG
	printf("Dentro reduccion:\nIterHilo:%dResultado:%fOperacion:%d",iterHilo,*resultado,operacion);
	#endif
	//Iteraciones que le toca a cada hilo
	int iterHilo=numElementos/numHilos;
	//Iteracion en la que comienza el hilo a trabajar
	int iteracionInicio=indiceHilo*iterHilo;
	//Iteracion en la que termina el hilo de trabajar
	int iteracionFin=iteracionInicio+iterHilo;
	//Iteraciones restantes
	int resto=numElementos%numHilos;
	
	//En las iteraciones correspondientes llama constantemente a la funcion operar y cada hilo guarda su resultado propio en la variable resultadoHilo 
	for (int i = iteracionInicio; i < iteracionFin; ++i)
	{
		(resultadoHilo) = operar(operacion,resultadoHilo,elementos[i]);
		#ifdef DEBUG
		printf("Dentro bucle: Resultado hilo:%f\n",resultadoHilo );
		#endif
	}
	//Uso esta variable auxiliar para que no se sobreescriba el valor cuando el hilo que termine primero realice las operaciones restantes
	double x=resultadoHilo;
	#ifdef DEBUG
	printf("soy thread %d\n",indiceHilo );
	#endif

	//Si no usamos una variable atomica se protege con el mutex el puntero a resultado para que varios hilos no puedan acceder a la vez
	#if !FEATURE_OPTIMIZE
	{
	std::lock_guard<std::mutex> lockm(g_m);
	//Utilizo la funcion abs porque en el caso de la resta al restar dos numeros negativos uno se convierte en positivo y en los demas casos es indiferente
	(*resultado)=operar(operacion,resultadoHilo,abs(*resultado));
	#ifdef DEBUG
	printf("%f\n",*resultado );
	#endif
	}
	#endif

	//Si usamos la variable atomica simplemente guardamos el resultado en ella
	#if FEATURE_OPTIMIZE
	resultadoAtomico=operar(operacion,resultadoAtomico,abs(resultadoHilo));
	#endif
	
	//Si el resto es distinto de 0 y ningun hilo ha hecho ya el resto el hilo que llega hace el resto
	if (resto!=0 && haceResto)
	{
			haceResto=false;
			resultadoHilo=0;
			iteracionInicio=numElementos-resto;
			iteracionFin=numElementos;
			for (int i = iteracionInicio; i < iteracionFin; ++i){
			(resultadoHilo) = operar(operacion,resultadoHilo,elementos[i]);
			#ifdef DEBUG
			printf("Dentro bucle: Resultado hilo:%f\n",resultadoHilo );
			#endif
			}
			
			//Se vuelve a proteger para que no accedan otros hilos a la vez
			#if !FEATURE_OPTIMIZE
			{
			std::lock_guard<std::mutex> lock(g_m);
			(*resultado)=operar(operacion,resultadoHilo,abs(*resultado));
			}
			#endif

			#if FEATURE_OPTIMIZE
			resultadoAtomico=operar(operacion,resultadoAtomico,abs(resultadoHilo));
			#endif
		

			#ifdef FEATURE_LOGGER
			arrayResultadosParciales[indiceHilo]=arrayResultadosParciales[indiceHilo]+resultadoHilo;
			#ifdef DEBUG
			printf("Resultado resto:%f\n",arrayResultadosParciales[indiceHilo] );
			#endif
			#endif
	}
	//Si hay hilo logger
	#ifdef FEATURE_LOGGER
	{
		resultadoHilo=x;
		//Protejo el acceso para guardar el resultado en el array global de resultados y para cuando el hilo notifique al logger
		std::lock_guard<std::mutex> locklg(g_m);
		if (operacion==0)
		{
			arrayResultadosParciales[indiceHilo]=(int)arrayResultadosParciales[indiceHilo]^(int)resultadoHilo;
		}else{
			arrayResultadosParciales[indiceHilo]=arrayResultadosParciales[indiceHilo]+resultadoHilo;
		}
		#ifdef DEBUG
		printf("Resultado parcial array:%f\n",arrayResultadosParciales[indiceHilo] );
		#endif
		g_datoALogger=true;
		//Antes de notificar aumento el contador de peticiones
		peticiones++;
		g_c.notify_one();
	}	
	#endif
}
#ifdef FEATURE_LOGGER
void funcion_logger(int numHilos,int operacion, double *arrayResultadosParciales){
	double solucionLogger=0;
	//Cuando las peticiones lleguen al número de hilos se acaba el bucle
	while (peticiones!=numHilos)
	{
		std::unique_lock<std::mutex> locklogger(g_m);
		g_c.wait(locklogger,[]{return g_datoALogger;});
		g_datoALogger=false;
		locklogger.unlock();
	}
	//Suma al array con los resultados de cada hilo
	if (operacion==0)
	{
		for (int i = 0; i < numHilos; ++i)
		{
			solucionLogger=(int)solucionLogger^(int)arrayResultadosParciales[i];
		}
	}else{
		for (int i = 0; i < numHilos; i++)
		{
			solucionLogger=solucionLogger+arrayResultadosParciales[i];	
			#ifdef DEBUG
			printf("solucion logger:%f\n",solucionLogger );
			#endif
		}
	}
	//Pasa el resultado al main
	resultadoLogger=solucionLogger;
}
#endif


int main(int argc, char* argv[]){
	//Obtiene el tiempo cuando se inicia la ejecucion
	gettimeofday(&tiempoInicio,NULL);
	//Los argumentos los guardo en variables para comodidad al programar
	int parametros=argc;	
	int numElementos = atoi(argv[1]);
	int numHilos;
	//Resultado de la operacion
	double resultado=0;
	//Array de elementos
	double *arrayElementos;
	//Array de los resultados parciales de cada hilo
	double *arrayResultadosParciales;
	//Operacion a realizar
	int operacion=-1;
	
	//Reservo espacio para el array de elementos	
    arrayElementos= (double *) (malloc(sizeof(double) * numElementos));
    //Inicializo el array de elementos
    for(int i =0; i< numElementos; i++){
      arrayElementos[i] =i;
      #ifdef DEBUG
      printf("Elemento array;%f\n",arrayElementos[i] );
      #endif
    }



	#ifdef DEBUG
	printf("Num parametros:%d\n", parametros);
	#endif
	if(parametros!=3 && parametros!=5){
	#ifdef DEBUG
	printf("Numero de parametros incorrecto\n");
	#endif
	return 0;
	}
	#ifdef DEBUG
	printf("Num elementos:%d\n", numElementos);	
	#endif

	//Caso multi thread
    if(parametros==5){
		if(strcmp(argv[3],"--multi-thread")!=0){
			#ifdef DEBUG
			printf("Error en el paso de parametros\n");
			#endif
			return 0;
		}
		numHilos=atoi(argv[4]);
		#ifdef DEBUG
		printf("Numero de Hilos:%d\n",numHilos );
		printf("trabajo:%d\n",numElementos/numHilos);
		#endif
		//Cambio la variable operacion para que los hilos sepan que operacion tienen que realizar
		if(strcmp(argv[2],"xor")==0){
			operacion=0;
			#ifdef DEBUG
			printf("Operacion xor\n");
			#endif		
		}else if(strcmp(argv[2],"sum")==0){
			operacion=1;
			#ifdef DEBUG		
			printf("Operacion sum\n");
			#endif
		}else if(strcmp(argv[2],"sub")==0){
			operacion=2;
			#ifdef DEBUG
			printf("Operacion sub\n");
			#endif
		}else{
			printf("Operacion desconocida\nFin del programa\n");		
			return 0;		
		}
		#ifdef DEBUG
		printf("Modo multi-thread\n");
		#endif
		
		//Reservo espacio para el array de resultados parciales
    	arrayResultadosParciales= (double *) (malloc(sizeof(double) * numHilos));
    	
  		//En caso de que haya logger se crea
    	#ifdef FEATURE_LOGGER
		std::thread logger(funcion_logger,numHilos,operacion,arrayResultadosParciales);
		#endif

		//Se crean tantos hilos como se haya indicado
		std::thread hilos[numHilos];
		for (int i = 0; i < numHilos; ++i)
		{	
			hilos[i]=std::thread(reduccion,arrayElementos,&resultado,operacion,numElementos, numHilos, i,arrayResultadosParciales);
		}
		
		//Se sincronizan los hilos
		for (int i = 0; i < numHilos; ++i)
		{
			hilos[i].join();
		}

		//Se sincroniza el logger
		#ifdef FEATURE_LOGGER
		logger.join();
		#endif
		#ifdef DEBUG
		printf("Resultado:%f\n",resultado);
		#ifdef FEATURE_LOGGER
		printf("Resultado logger:%f\n",resultadoLogger);
		#endif
		#if FEATURE_OPTIMIZE
		printf("Atomico:%f\n", resultadoAtomico.load());
		#endif
		#endif
		//Compara el resultado del hilo logger con la version paralela sin logger
		#ifdef FEATURE_LOGGER
		#if !FEATURE_OPTIMIZE
		if (resultado!=resultadoLogger)
		{
			printf("El resultado no coincide\n");
		}
		#endif
		#ifdef FEATURE_OPTIMIZE
		if (resultadoLogger!=resultadoAtomico)
		{
			printf("El resultado no coincide\n");
		}
		#endif
		
		#endif
	}
	//Version secuencial
	if(parametros==3){
		#ifdef DEBUG
		printf("Modo single thread\n");
		#endif
		//Realiza directamente la operacion sobre todo el bucle
		if(strcmp(argv[2],"xor")==0){
			#ifdef DEBUG
			printf("Operacion xor\n");
			#endif
			for(int i=0;i<numElementos;i++){
			resultado=(int)resultado^(int)arrayElementos[i];
			#ifdef DEBUG
			printf("%f\n",resultado );	
			#endif
			}
			#ifdef DEBUG
			printf("Resultado: %f\n",resultado);	
			#endif
		}else if(strcmp(argv[2],"sum")==0){
			#ifdef DEBUG		
			printf("Operacion sum\n");
			#endif
			for(int i=0;i<numElementos;i++){
			resultado=resultado+arrayElementos[i];
			#ifdef DEBUG
			printf("%f\n",resultado );
			#endif
			}
			#ifdef DEBUG
			printf("Resultado: %f\n",resultado );
			#endif
		}else if(strcmp(argv[2],"sub")==0){
			#ifdef DEBUG
			printf("Operacion sub\n");
			#endif
			for(int i=0;i<numElementos;i++){
			resultado=resultado-arrayElementos[i];
			#ifdef DEBUG
			printf("%f\n",resultado );
			#endif
			}
		}else{
			printf("Operacion desconocida\nFin del programa\n");		
			return 0;		
		}
	}
	//Obtiene el tiempo cuando se finaliza la ejecucion
	gettimeofday(&tiempoFin,NULL);
	//Resta los 2 tiempos y muestra el resultado por pantalla 
	double tiempo=(tiempoFin.tv_sec-tiempoInicio.tv_sec)+(tiempoFin.tv_usec-tiempoInicio.tv_usec)/1000000.0;
	printf("%f \n",tiempo);
}