#!/usr/bin/env bash
size=1000000
size2=50000000
size3=200000000
op=sub
nthreads=4
case "$1" in
    st1)
        ./build/p1 $size $op
        ;;
    mt1)
        ./build/p1 $size $op --multi-thread $nthreads
        ;;
    st2)
        ./build/p1 $size2 $op
        ;;
    mt2)
        ./build/p1 $size2 $op --multi-thread $nthreads
        ;;
    st3)
        ./build/p1 $size3 $op
        ;;
    mt3)
        ./build/p1 $size3 $op --multi-thread $nthreads
        ;;
esac
