# P1: Multithreading en C++

Guzmán Gutiérrez Fernández

Fecha 28/11/2019

## Prefacio

Objetivos conseguidos

Breve descripción de lo que te ha parecido la práctica. Aprendizajes, dificultades, cosas interesantes, qué te hubiera gustado hacer o mejorarías, qué conceptos te gustaría haber practicado de C++/OMP, opinión sobre el informe, etc

Me parece una buena práctica para comprender el comportamiento de los hilos antes de comenzar a usar openmp. La mayor dificultad que he encontrado en esta práctica es el logger y utilizar el lenguaje c ya que estoy poco familiarizado con él. 

## Índice

1. Sistema
2. Diseño e Implementación del Software
3. Metodología y desarrollo de las pruebas realizadas
4. Discusión

## 1. Sistema

Arquitectura:          x86_64
modo(s) de operación de las CPUs:32-bit, 64-bit
Orden de bytes:        Little Endian
CPU(s):                4
On-line CPU(s) list:   0-3
Hilo(s) de procesamiento por núcleo:2
Núcleo(s) por «socket»:2
Socket(s):             1
Modo(s) NUMA:          1
ID de fabricante:      GenuineIntel
Familia de CPU:        6
Modelo:                78
Model name:            Intel(R) Core(TM) i7-6500U CPU @ 2.50GHz
Revisión:             3
CPU MHz:               2713.146
CPU max MHz:           3100,0000
CPU min MHz:           400,0000
BogoMIPS:              5184.00
Virtualización:       VT-x
Caché L1d:            32K
Caché L1i:            32K
Caché L2:             256K
Caché L3:             4096K
NUMA node0 CPU(s):     0-3
Flags:                 fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx pdpe1gb rdtscp lm constant_tsc art arch_perfmon pebs bts rep_good nopl xtopology nonstop_tsc aperfmperf tsc_known_freq pni pclmulqdq dtes64 monitor ds_cpl vmx est tm2 ssse3 sdbg fma cx16 xtpr pdcm pcid sse4_1 sse4_2 x2apic movbe popcnt tsc_deadline_timer aes xsave avx f16c rdrand lahf_lm abm 3dnowprefetch epb intel_pt tpr_shadow vnmi flexpriority ept vpid fsgsbase tsc_adjust bmi1 avx2 smep bmi2 erms invpcid mpx rdseed adx smap clflushopt xsaveopt xsavec xgetbv1 xsaves dtherm ida arat pln pts hwp hwp_notify hwp_act_window hwp_epp

La frecuencia de los procesadores al ejecutar las pruebas está al minimo, es decir 400MHz


## 2. Diseño e Implementación del Software
Las variables globales utilizadas son un mutex para restringir el acceso a la variable resultado, en caso de que haya logger al array de resultados parciales también lo protege este mutex. Si el hilo logger está activado también utilizo una variable condicional, un booleano que indica cuando un hilo está listo para notificar al logger, y un double que utilizo para enviar el resultado de la función del logger al main. Si está activado FEATURE OPTIMIZE creo una variable atómica para usarla en lugar de proteger el acceso a la variable resultado con el mutex. 

El programa cuenta con 4 funciones:

	operar: Realiza la operación indicada como argumento entre 2 valores también pasados como argumento, con el valor 0 en el primer argumento hace la operación Xor, con 1 Suma y con 2 Resta.

	reducción: Es la función que realizan los hilos creados, se distribuyen las iteraciones del bucle que realiza ooperaciones entre el número de hilos que tenga el programa de forma equitativa, en caso de que no puedan ser equitativas se asignan las iteraciones sobrantes al hilo que primero termine de hacer sus operaciones. Para hacer la distribución del trabajo he hecho que a esta función haya que pasarle el número de elementos a operar, el número de hilos utilizado y el índice del hilo. De esta forma las iteraciones que hace cada hilo es la división entera del número de elementos entre el número de hilos, la iteración inicial del hilo es igual a su índice por el número de iteraciones y la iteración final es la inicial mas el número de iteraciones de cada hilo, y las iteraciones que sobran son el resto de dividir el número de elementos entre el número de hilos. Con estos datos he hecho un bucle que vaya desde la iteración inicial a la final llamando a la función operar guardando el resultado en una variable local del hilo que guarda el resultado de sus iteraciones. Después de obtener el resultado local he utilizado un mutex para proteger la variable resultado y que los hilos no accedan a la vez a ella, ya que es la que pasa el resultado al main. Después utilizo un if que comprueba si el resto del número de elementos entre el número de hilos es distinto de 0 y ningún hilo ha entrado a ese if antes que él, utilizo como variable global un booleano que el primer hilo cambia justo al entrar en el if para que los demás no entren, el hilo que haya entrado al if llama a la operar en un bucle que utiliza como iteración inicial el número de elementos menos el resto y como iteración final el número de elementos. En caso de que este activado FEATURE OPTIMIZE en lugar de proteger la variable resultado con un mutex simplemente modificamos el valor de la variable atómica. Si está activado FEATURE LOGGER cada hilo guardará su resultado parcial en el array global de resultados, sumar 1 al contador global de peticiones y notificar al logger. 

	funcion_logger: Esta funcion está esperando constantemente hasta que reciba un número de peticiones igual al número de hilos, lo cual significara que todos los hilos ya han colocado su resultado en el array de resultados, posteriormente realiza la operación correspondiente con el array de resultados parciales y envía el resultado al main a través de otra variable global.

	main: Primero procesa los argumentos, en caso de que se indique crea el hilo logger, después crea los hilos que ejecutan la función reducción, posteriormente utiliza join para sincronizar los hilos y por último compara el resultado del logger con el obtenido sin el hilo logger, muestra por pantalla el tiempo que ha tardado en ejecutarse el programa.


## 3. Metodología y desarrollo de las pruebas realizadas

Los scripts que he utilizado han sido los siguientes:

benchmark.sh:
#!/usr/bin/env bash
file=results1.log
touch $file
for season in 1 2; do
    for benchcase in st1 mt1 st2 mt2 st3 mt3; do
        echo $benchcase >> $file
        for i in `seq 1 1 10`;
        do
            ./benchsuite.sh $benchcase >> $file # results dumped
            sleep 1
        done
    done
done

benchsuite.sh:
#!/usr/bin/env bash
size=1000000
size2=50000000
size3=200000000
op=sub
nthreads=4
case "$1" in
    st1)
        ./build/p1 $size $op
        ;;
    mt1)
        ./build/p1 $size $op --multi-thread $nthreads
        ;;
    st2)
        ./build/p1 $size2 $op
        ;;
    mt2)
        ./build/p1 $size2 $op --multi-thread $nthreads
        ;;
    st3)
        ./build/p1 $size3 $op
        ;;
    mt3)
        ./build/p1 $size3 $op --multi-thread $nthreads
        ;;
esac

El procedimiento usado ha sido ejecutar el sript benchmark.sh 1 vez sin coger los resultados a modo de warmup, en todas las siguientes ejecuciones se ha usado el tamaño indicado en size, size2 y size3, he ejecutado el script varias veces cambiando el número de threads a los valores 2 3 y 4 respectivamente y guardado los resultados obtenidos en un documento odt. En la carpeta Datos se encuentra el documento odt con los resultados obtenidos del benchmarking en la segunda estación con las gráficas correspondientes al speedup en función del tamaño del array y del número de hilos utilizado. En la hoja 1 del documento se encuentran los datos sin FEATURE OPTIMIZE y en la hoja 2 los datos con FEATURE OPTIMIZE, al utilizar FEATURE OPTIMIZE se ha obtenido un speedup ligeramente mayor.

![](speedups.png)
![](speedupsoptimize.png)

En las gráficas se aprecia que la versión paralela obtiene mayor speedup con respecto a la versión secuencial en función del tamaño del array de elementos, cuanto mayor sea el array mayor es el beneficio de la versión paralela, tambien se puede ver que con mayor número de hilos obtenemos más beneficios. A pesar de esto los resultados no han sido los esperados debido a que al utilizar 2 hilos la parte paralela es más lenta que la secuencial y el speedup obtenido al usar 4 hilos es bastante bajo con respecto al que me esperaba obtener. Supongo que para tamaños mayores independientemente del número de hilos acabe obteniendo un tiempo menor que la versión secuencial pero no he utilizado valores mas altos en el tamaño del array para seguir comparando porque el tiempo que tardaban en ejecutar los scripts de benchmarking era excesivo.

## 4. Discusión
La mayoria de problemas con esta práctica han sido principalmente por no tener la suficiente soltura programando en c mas que por la forma de gestionar los hilos, después de haber realizado muchos programas sencillos para coger soltura con c realizar la parte base de la práctica no ha sido desmasiado complicado.

Después tenía un problema con el logger porque hice un bucle for desde 0 hasta el número de hilos, pero eso solamente funcionaría en el caso ideal, es decir, un hilo manda una petición el logger pasa el wait, después otro hilo manda la petición, vuelve a pasar el wait y así sucesivamente, pero si este caso ideal no ocurre puede ser que el wait no se pase un número de veces igual al número de hilos y se quede esperando infinitamente haciendo que el bucle no llegue nunca al final, esto lo he solucionado cambiando el for por un while que se ejecuta hasta que la variable global peticiones sea igual al número de hilos, cada hilo aumenta en 1 esta variable antes de notificar al logger.

El mayor problema que he tenido ha sido el benchmarking, ya que como he indicado el resultado no es el que me esperaba. Al ver tan poco speedup pense que podía ser porque media el tiempo de ejecución de todo el programa completo, probé a medir solamente la región de interés (la parte en la que trabajan los hilos en el código paralelo y la que se realizan las operaciones en el secuencial) y el speedup obtenido pasaba de 1.2 a 1.5, al ver que la diferencia no era demasiado significativa no volvi a realizar el benchmarking por el coste de tiempo que supone cambiar todo otra vez. 

La parte de utilizar una variable atómica se hace rápido una vez que sabes lo que hay que hacer, pero he tardado bastante tiempo en saber que la mejora era cambiar la variable protegida por un mutex por un atómico.

No he hecho la parte del conector de java por el tiempo que he perdido cambiando el codigo para intentar aumentar el speedup.
