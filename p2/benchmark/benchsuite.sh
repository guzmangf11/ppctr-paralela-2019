#!/usr/bin/env bash
size=500000
size2=5000000
size3=10000000
size4=100000000
case "$1" in
  a1)
  ./build/seq $size
  ;;
  b1)
  NTHREADS=2 ./build/cpp $size
  ;;
  c1)
  NTHREADS=4 ./build/cpp $size
  ;;
  d1)
  NTHREADS=2 ./build/omp $size
  ;;
  e1)
  NTHREADS=4 ./build/omp $size
  ;;
  a2)
  ./build/seq $size2
  ;;
  b2)
  NTHREADS=2 ./build/cpp $size2
  ;;
  c2)
  NTHREADS=4 ./build/cpp $size2
  ;;
  d2)
  NTHREADS=2 ./build/omp $size2
  ;;
  e2)
  NTHREADS=4 ./build/omp $size2
  ;;
  a3)
  ./build/seq $size3
  ;;
  b3)
  NTHREADS=2 ./build/cpp $size3
  ;;
  c3)
  NTHREADS=4 ./build/cpp $size3
  ;;
  d3)
  NTHREADS=2 ./build/omp $size3
  ;;
  e3)
  NTHREADS=4 ./build/omp $size3
  ;;
  a4)
  ./build/seq $size4
  ;;
  b4)
  NTHREADS=2 ./build/cpp $size4
  ;;
  c4)
  NTHREADS=4 ./build/cpp $size4
  ;;
  d4)
  NTHREADS=2 ./build/omp $size4
  ;;
  e4)
  NTHREADS=4 ./build/omp $size4
  ;;
esac
