#!/usr/bin/env bash
file=benchmark/results1.log
touch $file
for season in 1; do
  for benchcase in a1 b1 c1 d1 e1 a2 b2 c2 d2 e2 a3 b3 c3 d3 e3 a4 b4 c4 d4 e4; do
    echo $benchcase >> $file
    for i in `seq 1 1 16`;
    do
      ./benchmark/benchsuite.sh $benchcase >> $file 
      sleep 1
    done
  done
done
