# P2:Optimizing

Guzmán Gutierrez y Alba Rivero

Fecha 20/12/2019

## Prefacio
Hemos conseguido terminar todos los apartados de la práctica. No hemos intentado hacer los de la parte de competición.

La práctica nos ha parecido buena como transición de C++ a OpenMP ya que la paralelización en C++ es simplemente un repaso porque que ya lo hemos trabajado mucho en la 1 y la parte de OpenMP es muy básica ya que la distribución del trabajo lo seguimos haciendo nosotros y lo único que utilizamos de OpenMP es la forma de crear los hilos con la región paralela y algunas funciones del runtime para gestionar los lock y obtener el tiempo de ejecución. En esta práctica hemos aprendido a pasar parámetros mediante variables de entorno ya que nunca lo habíamos trabajado antes y nos ha parecido interesante.


## Índice

1. Sistema
2. Diseño e Implementación del Software
3. Metodología y desarrollo de las pruebas realizadas


## 1. Sistema

Processor		Intel(R) Core(TM) i7-7500U CPU @ 2.70GHz

Memory			8048MB (2359MB used)

Machine Type		Notebook

Operating System	Ubuntu 18.04.3 LTS

Kernel			Linux 5.0.0-32-generic (x86_64)

Version			#34~18.04.2-Ubuntu SMP Thu Oct 10 10:36:02 UTC 2019

C Library		GNU C Library / (Ubuntu GLIBC 2.27-3ubuntu1) 2.27

Distribution		Ubuntu 18.04.3 LTS

Topology 		1 physical processor; 2 cores; 4 threads

Logical CPU Config 	4X 2700,00 MHz

Clocks 			400,00-2700,00 1x

Caches-
	Level 1(Data) 		2x 32KB (64KB), 8-way set-associative, 64 sets
	Level 1(Instruction) 	2x 32KB (64KB), 8-way set-associative, 64 sets
	Level 2(Unified) 	2x 256KB (512KB), 4-way set-associative, 1024 sets
	Level 3(Unified) 	1x 4096KB (4096KB), 16-way set-associative, 4096 sets

CPU flags: x86-64 fpu fpu_exception wp vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca 		      cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx pdpe1gb 	      rdtscp constant_tsc art arch_perfmon pebs bts rep_good nopl xtopology nonstop_tsc 	      cpuid aperfmperf tsc_known_freq pni pclmulqdq dtes64 monitor ds_cpl vmx est tm2 		      ssse3 sdbg fma cx16 xtpr pdcm pcid sse4_1 sse4_2 x2apic movbe popcnt 		      tsc_deadline_timer aes xsave avx f16c rdrand lahf_lm abm 3dnowprefetch cpuid_fault 	       epb invpcid_single pti ssbd ibrs ibpb stibp tpr_shadow vnmi flexpriority ept vpid 	      ept_ad fsgsbase tsc_adjust bmi1 avx2 smep bmi2 erms invpcid mpx rdseed adx smap 		      clflushopt intel_pt xsaveopt xsavec xgetbv1 xsaves dtherm ida arat pln pts hwp 		      hwp_notify hwp_act_window hwp_epp md_clear flush_l1d cpufreq


## 2. Diseño e Implementación del Software
Para la realización de la practica he seguido el siguiente orden:
	1. Parte Basica
	2. C++
	3. OpenMP

1. Parte Basica:

Lo primero es comprobar el paso de parametros, la practica solo puede recibir dos o tres parametros siendo el tercero opcional en el caso secuencial, en caso contrario salta un mensaje de error.
En caso de ser paralelo si no le paso el numero de threads como argumento debo leerlo de la variable de entorno de este modo:

```
const char* nthreads="NTHREADS";
numThreads=atoi(getenv(nthreads));
```

Para realizar la compilacion condicional hemos declarado una variable de preprocesado llamado SCO que dependiendo de su valor se compilara una parte diferente del codigo.

Despues hemos realizado un makefile desde el cual le doy valor a SCO y asi compilar y ejecutar la parte correspondiente
```
mkdir -p build; \
g++ -std=c++11 -D SCO="X" -o build/"X" src/p2.cpp -fopenmp;
```

En el caso de SCO = 0 se ejecuta la parte secuencial, esta parte realiza el bucle el numero de iteraciones que paso como parametro y se muestra por pantalla el resultado obtenido.


 2. C++ :

 En el caso de querer ejecutar el codigo de la parte paralelizada con c++ debo hacerlo con el SCO = 1. En este caso debo coger el numero de threads que paso mediante la CLI, y en caso de no pasarla la obtendo de la variable de entorno, una vez obtenido el numero de threads , creo los threads y calculo el trabajo que va a realizar cada uno, el primer hilo en acabar será el que se ocupara del resto, en caso de que este existiese. Cada thread llama a la funcion paralel la cual se encarga de calcular el resultado en la variable ellipsepar protegida por un mutex para que no puedan acceder varios threads a la vez.

 3. OpenMP:

 Por ultimo, el caso OpenMP para el cual a SCO le damos valor 2. Obtengo el numero de threads igual que en el caso anterior.
 Lo primero que hago es crear un lock e inicializarle , los bloqueos del mutex se utilizan con la misma finalidad que la parte de C++ cojo el control del lock a la hora de calcular el resultado y una vez que he terminado destruyo el lock. Para establecer el reparto de iteraciones utilizo la funcion omp_get_thread_num() para obtener el indice de cada hilo y asignarle el trabajo correspondiente.
 ```
 // creacion e inicializacion del lock.
 omp_lock_t lock;
 omp_init_lock(&lock);

 //cojo el control del lock.
 omp_set_lock(&lock);

 //libero el lock.
 omp_unset_lock(&lock);

 //destrucción del lock.
 omp_destroy_lock(&lock);
 ```

La region paralela esta definida por la directiva parallel `#pragma omp parallel{}`, la cual crea un equipo de threads (tantos como se hayan indicado) que ejecutaran el código comprendido en la región paralela.


## 3. Metodología y desarrollo de las pruebas realizadas

La primera prueba que he realizado es comprobar que la ejecucion de las diferentes partes del programa me proporcionan el mismo resultado.

Una vez comprobado que el resultado de las diferentes implementaciones es correcto hemos ejecutado los scripts.

results.sh
#!/usr/bin/env bash
file=benchmark/results1.log
touch $file
for season in 1; do
  for benchcase in a1 b1 c1 d1 e1 a2 b2 c2 d2 e2 a3 b3 c3 d3 e3 a4 b4 c4 d4 e4; do
    echo $benchcase >> $file
    for i in `seq 1 1 16`;
    do
      ./benchmark/benchsuite.sh $benchcase >> $file # results dumped
      sleep 1
    done
  done
done

benchsuite.sh
#!/usr/bin/env bash
size=500000
size2=5000000
size3=10000000
size4=100000000
case "$1" in
  a1)
  ./build/seq $size
  ;;
  b1)
  NTHREADS=2 ./build/cpp $size
  ;;
  c1)
  NTHREADS=4 ./build/cpp $size
  ;;
  d1)
  NTHREADS=2 ./build/omp $size
  ;;
  e1)
  NTHREADS=4 ./build/omp $size
  ;;
  a2)
  ./build/seq $size2
  ;;
  b2)
  NTHREADS=2 ./build/cpp $size2
  ;;
  c2)
  NTHREADS=4 ./build/cpp $size2
  ;;
  d2)
  NTHREADS=2 ./build/omp $size2
  ;;
  e2)
  NTHREADS=4 ./build/omp $size2
  ;;
  a3)
  ./build/seq $size3
  ;;
  b3)
  NTHREADS=2 ./build/cpp $size3
  ;;
  c3)
  NTHREADS=4 ./build/cpp $size3
  ;;
  d3)
  NTHREADS=2 ./build/omp $size3
  ;;
  e3)
  NTHREADS=4 ./build/omp $size3
  ;;
  a4)
  ./build/seq $size4
  ;;
  b4)
  NTHREADS=2 ./build/cpp $size4
  ;;
  c4)
  NTHREADS=4 ./build/cpp $size4
  ;;
  d4)
  NTHREADS=2 ./build/omp $size4
  ;;
  e4)
  NTHREADS=4 ./build/omp $size4
  ;;
esac

Los resultados obtenidos con los sripts son los siguientes:
<img src="images/datos.png">

Graficas:
La primera gráfica indica los speedup con diferentes números de iteraciones. Los bloques indican la forma de ejecucion, que ha sido secuencial 2 threads en c++, 4 threads en c++, 2 hilos en openmp y 4 hilos en openmp respectivamente.
<img src="images/Speed.png">

--------------------
<img src="images/tiempo.png">



