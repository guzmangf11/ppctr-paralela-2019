#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <thread>
#include <mutex>
#include "p2.hpp"
#include <omp.h>

  #if SCO!=0
  int numThreads;
  #endif
  double gap;
  long numIts;
  #if SCO!=0
  bool haceResto=true;
  #endif
  //#define RESUL

void parse(int argc, char* argv[]){
  // parse 'long' and set numIts
  #if SCO!=0
  if(getenv("NTHREADS")){
    const char* nthreads="NTHREADS";
    numThreads=atoi(getenv(nthreads));
  }
  #endif
  if (argc!=2 && argc!=3) {
    printf("Numero incorrecto de argumentos\n");
  }
  if (argc==3){
    int nthreads = atoi(argv[2]);
  }
  numIts=atol(argv[1]);
  gap = 1.0/numIts;
  if (numIts<=0){
    fprintf(stderr, "error: invalid iterations\n");
    exit(-1);
  };
}
#if SCO==0
void sequential(){
  double ellipse=0;
      for (int i=0; i<numIts; i++) {
        ellipse += 4/(1+((i+0.5)*gap)*((i+0.5)*gap));
  }
  ellipse *= 2*gap/sqrt((4*A*C-B*B));

  #ifdef RESUL
  printf("ellipse: %f \n", ellipse);
  #endif
}
#endif

#if SCO==1
void paralel(int indice,double *ellipsepar, std::mutex *g_m){
  if (indice==0)
  {
    printf("Paralelo c++\n");
  }
    double resultadoHilo=0;


  int iterHilo=numIts/numThreads;
  //Iteracion en la que comienza el hilo a trabajar
  int iteracionInicio=indice*iterHilo;
  //Iteracion en la que termina el hilo de trabajar
  int iteracionFin=iteracionInicio+iterHilo;
  //Iteraciones restantes
  int resto=numIts%numThreads;

  for (int i = iteracionInicio; i < iteracionFin; i++)
  {
    resultadoHilo += 4/(1+((i+0.5)*gap)*((i+0.5)*gap));
  }
  resultadoHilo *= 2*gap/sqrt((4*A*C-B*B));
  {
  std::lock_guard<std::mutex> lock(*g_m);
  (*ellipsepar)+=resultadoHilo;
  }
  if (resto!=0 && haceResto)
  {
      haceResto=false;
      resultadoHilo=0;
      iteracionInicio=numIts-resto;
      iteracionFin=numIts;
      for (int i = iteracionInicio; i < iteracionFin; i++){
      resultadoHilo += 4/(1+((i+0.5)*gap)*((i+0.5)*gap));
      }
      resultadoHilo *= 2*gap/sqrt((4*A*C-B*B));
      {
      std::lock_guard<std::mutex> lock(*g_m);
      (*ellipsepar)+=resultadoHilo;
      }
  }
}

#endif

#if SCO==2
void paralelOmp(double *ellipsepar){
  printf("Paralelo omp\n");
  omp_lock_t lock;
  omp_init_lock(&lock);
  #pragma omp parallel
  {
  int indice=omp_get_thread_num();
  double resultadoHilo=0;


  int iterHilo=numIts/numThreads;
  //Iteracion en la que comienza el hilo a trabajar
  int iteracionInicio=indice*iterHilo;
  //Iteracion en la que termina el hilo de trabajar
  int iteracionFin=iteracionInicio+iterHilo;
  //Iteraciones restantes
  int resto=numIts%numThreads;


  for (int i = iteracionInicio; i < iteracionFin; i++)
  {
    resultadoHilo += 4/(1+((i+0.5)*gap)*((i+0.5)*gap));
  }
  resultadoHilo *= 2*gap/sqrt((4*A*C-B*B));
  omp_set_lock(&lock);
  (*ellipsepar)+=resultadoHilo;
  omp_unset_lock(&lock);
  if (resto!=0 && haceResto)
  {
      haceResto=false;
      resultadoHilo=0;
      iteracionInicio=numIts-resto;
      iteracionFin=numIts;
      for (int i = iteracionInicio; i < iteracionFin; i++){
      resultadoHilo += 4/(1+((i+0.5)*gap)*((i+0.5)*gap));
      }
      resultadoHilo *= 2*gap/sqrt((4*A*C-B*B));
      omp_set_lock(&lock);
      (*ellipsepar)+=resultadoHilo;
      omp_unset_lock(&lock);
  }
}
  omp_destroy_lock(&lock);
}
#endif
/*
La función getenv rastrea el entorno en busca de una cadena con el nombre proporcionado y envía un valor asociado a dicho nombre.
*/
int main(int argc, char* argv[])
{
    double tiempoInicio=omp_get_wtime();

    parse(argc, argv);
    #if SCO == 0
    sequential();
    #endif
    double ellipsepar=0;
    #if SCO == 1
    std::mutex g_m;
    std::thread threads[numThreads];
    for (int i = 0; i < numThreads; i++)
    {
      threads[i]=std::thread(paralel,i,&ellipsepar,&g_m);
    }
    for (int i = 0; i < numThreads; i++)
    {
      threads[i].join();
    }
    #ifdef RESUL
    printf("ellipse:%f\n",ellipsepar );
    #endif
    #endif

    #if SCO == 2
    paralelOmp(&ellipsepar);
    #ifdef RESUL
    printf("ellipse:%f\n",ellipsepar);
    #endif
    #endif
    double tiempoFin=omp_get_wtime();
    printf("Tiempo:%f\n",tiempoFin-tiempoInicio);
}
