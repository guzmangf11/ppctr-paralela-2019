# P5: Fractal de Mandelbrot

Autor: Guzmán Gutiérrez Fernández

Fecha 19/12/2019

## Índice

1. Sistema
2. Diseño e Implementación del Software
3. Metodología y desarrollo de las pruebas realizadas
4. Discusión

##Prefacio 
Esta práctica me ha resultado útil para valorar la importancia del profiling de un programa ya que es de gran ayuda a la hora de saber por donde comenzar a paralelizar.

## 1. Sistema

Arquitectura:          x86_64
modo(s) de operación de las CPUs:32-bit, 64-bit
Orden de bytes:        Little Endian
CPU(s):                4
On-line CPU(s) list:   0-3
Hilo(s) de procesamiento por núcleo:2
Núcleo(s) por «socket»:2
Socket(s):             1
Modo(s) NUMA:          1
ID de fabricante:      GenuineIntel
Familia de CPU:        6
Modelo:                78
Model name:            Intel(R) Core(TM) i7-6500U CPU @ 2.50GHz
Revisión:             3
CPU MHz:               2713.146
CPU max MHz:           3100,0000
CPU min MHz:           400,0000
BogoMIPS:              5184.00
Virtualización:       VT-x
Caché L1d:            32K
Caché L1i:            32K
Caché L2:             256K
Caché L3:             4096K
NUMA node0 CPU(s):     0-3
Flags:                 fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx pdpe1gb rdtscp lm constant_tsc art arch_perfmon pebs bts rep_good nopl xtopology nonstop_tsc aperfmperf tsc_known_freq pni pclmulqdq dtes64 monitor ds_cpl vmx est tm2 ssse3 sdbg fma cx16 xtpr pdcm pcid sse4_1 sse4_2 x2apic movbe popcnt tsc_deadline_timer aes xsave avx f16c rdrand lahf_lm abm 3dnowprefetch epb intel_pt tpr_shadow vnmi flexpriority ept vpid fsgsbase tsc_adjust bmi1 avx2 smep bmi2 erms invpcid mpx rdseed adx smap clflushopt xsaveopt xsavec xgetbv1 xsaves dtherm ida arat pln pts hwp hwp_notify hwp_act_window hwp_epp

La frecuencia de los procesadores al ejecutar las pruebas está al minimo, es decir 400MHz

## 2. Diseño e Implementación del Software
He utilizado compilación condicional para obtener distintas implementaciones del programa. Dependiendo del valor de MODE se compilara una u otra.

Si MODE=0 es la versión original, la única sin paralelizar
Si MODE=1 es la paralelización de la versión original basada en el profiling, es decir con el bucle de llamadas a la función explode paralelizado. Esta versión también es la que implementa la región crítica de forma secuencial.
Si MODE=2 es la paralelización con la región crítica implementada con las directivas omp. Utilizo critical para proteger la variable c_max.
Si MODE=3 la región crítica está implementada con las funciones de runtime de openmp(sus lock).
Si MODE=4 la región crítica utiliza variables privadas a cada thread y selecciona el máximo gracias a la directiva reduction(max:c_max).

## 3. Metodología y desarrollo de las pruebas realizadas
nota: Las ejecuciones se han realizado con los valores de count_max=1000 y n=3000

Primero he realizado ejecuciones separadas de todas las implementaciones distintas para comprobar que pintaban el fractal correctamente. Una vez visto que los resultados obtenidos son correctos he ejecutado el script benchmark.sh el cual llama a benchsuite.sh 10 veces en 2 estaciones y he tomado los resultados obtenidos de la segunda estación.

benchmark.sh:
#!/usr/bin/env bash
file=results5.log
touch $file
for season in 1 2; do
	for i in `seq 1 1 10`;
        do
            ./benchsuite.sh $benchcase >> $file # results dumped
            sleep 1
        done
done

benchsuite.sh:
#!/usr/bin/env bash

./build/p5
./build/p5seq
./build/p5omp
./build/p5run
./build/p5var

Los resultados se encuentran en el fichero results5.log. Las gráficas que he realizado me ha parecido más oportuno colocarlas en el apartado de discusión como respuesta al ejercicio 3 ya que es en el que hay que realizar las distintas implementaciones. Una vez obtenidos estos resultados he realizado otro script llamado benchsuite2.sh para medir el tiempo de ejecución de la versión secuencial y la paralela realizada para el ejercicio 2 en lugar de solamente la región crítica.
He cambiado el script benchmark.sh para que llame a este nuevo script.

benchmark.sh:
#!/usr/bin/env bash
file=results5-2.log
touch $file
for season in 1 2; do
	for i in `seq 1 1 10`;
        do
            ./benchsuite2.sh $benchcase >> $file # results dumped
            sleep 1
        done
done

benchsuite2.sh:
#!/usr/bin/env bash

./build/p5
./build/p5seq

Los resultados son los siguientes(También se pueden ver en el fichero results5-2.log)

Versión secuencial:
15,577974
15,850492
15,964827
15,591066
15,588974
15,61176
15,619834
15,705146
15,677947
15,599736
La media de las ejecuciones secuenciales es: 15,6787756

Versión paralela:
8,078129
8,062498
8,207676
8,120553
8,055731
8,065717
8,068664
8,112744
8,086181
8,052747
La media de las ejecuciones paralelas es: 8,091064

Por tanto el speedup obtenido solamente con la paralelización de las llamadas a explode localizadas con el profiling del programa es 1,9377890967. 

## 4. Discusión

## Ejercicio 1
Al realizar el profiling vi que el tiempo de ejecución del programa era excesivo por tanto lo primero que hice fue analizar el código del programa para reducir el tamaño del mismo y realizar el profiling sin perder tanto tiempo. Detecte que el tamaño del problema depende de dos variables, "count_max" y "n" que determina el número de pixeles.

Primero cambie los valores de count_max a 10 y el número de pixeles a 81. Obtuve este resultado

![](81px_10n.png)

En esta imagen se aprecia que el mayor coste es en la función ppma_write()

Volvi a cambiar los valores para ver como cambiaba el analisis. Esta vez con count_max a 100 y los pixeles a 81. El resultado es este

![](81px_100n.png)

Esta vez al ver el código fuente se aprecia que el main hace 6561 llamadas a la función explode y supone un coste bastante significativo, mientras que la otra función que supone un gran coste(ppma_write()) solo es llamada una vez, por tanto vamos a centrarnos en paralelizar las llamadas a la función explode.

##Ejercicio 2

1. Todas las directivas openmp para este apartado han sido utilizadas en practicas anteriores.

2. 
 - private(i,j,x,y): Porque cada hilo ha de tener su propia copia de estas variables para que otros hilos no alteren su valor.
 - shared(x_min,xmax): Porque han de tener el mismo valor para todos los hilos.

3. El paralelismo en este programa se consigue utilizando la directiva for en el bucle de llamadas a la función explode para distribuir las iteraciones del bucle entre los hilos, ha sido la única paralelización que he realizado, me he centrado en ella para este apartado porque con el profiling he identificado que el cuello de botella se encontraba en las llamadas a esta función, ya que a pesar de que haya más bucles se ha visto en el profiling que su coste no es relevante.

##Ejercicio 3

![](tiempos_region_critica.png)

En esta gráfica se muestra la media del tiempo de 10 ejecuciones de la sección crítica de código con las distintas implementaciones usando el script benchsuite2.sh.

Para comparar las distintas implementaciones he hecho una grafica que represente el speedup de las demás versiones con la realizada mediante funciones del runtime de openmp ya que es la que más tarda.

![](speedup.png)

Las conclusiones que he sacado de esto es que la forma más rapida de implementarlo es utilizando reduction() y que la versión secuencial es mas rapida que las paralelas con runtime de openmp y con la directiva critical. Esto se debe a que como hemos visto en prácticas anteriores en las que teníamos que establecer millones de iteraciones para ver mejora en la versión paralela, cuanto mayor sea el número de iteraciones mayor es el beneficio de paralelizar el programa.



















