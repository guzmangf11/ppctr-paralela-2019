# Filtrado de un Vídeo mediante Tareas OpenMP

Guzmán Gutiérrez Fernández

Fecha 18/12/2019


## Índice

1. Sistema
2. Diseño e Implementación del Software
3. Metodología y desarrollo de las pruebas realizadas
4. Discusión

## 1. Sistema

Arquitectura:          x86_64
modo(s) de operación de las CPUs:32-bit, 64-bit
Orden de bytes:        Little Endian
CPU(s):                4
On-line CPU(s) list:   0-3
Hilo(s) de procesamiento por núcleo:2
Núcleo(s) por «socket»:2
Socket(s):             1
Modo(s) NUMA:          1
ID de fabricante:      GenuineIntel
Familia de CPU:        6
Modelo:                78
Model name:            Intel(R) Core(TM) i7-6500U CPU @ 2.50GHz
Revisión:             3
CPU MHz:               2713.146
CPU max MHz:           3100,0000
CPU min MHz:           400,0000
BogoMIPS:              5184.00
Virtualización:       VT-x
Caché L1d:            32K
Caché L1i:            32K
Caché L2:             256K
Caché L3:             4096K
NUMA node0 CPU(s):     0-3
Flags:                 fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx pdpe1gb rdtscp lm constant_tsc art arch_perfmon pebs bts rep_good nopl xtopology nonstop_tsc aperfmperf tsc_known_freq pni pclmulqdq dtes64 monitor ds_cpl vmx est tm2 ssse3 sdbg fma cx16 xtpr pdcm pcid sse4_1 sse4_2 x2apic movbe popcnt tsc_deadline_timer aes xsave avx f16c rdrand lahf_lm abm 3dnowprefetch epb intel_pt tpr_shadow vnmi flexpriority ept vpid fsgsbase tsc_adjust bmi1 avx2 smep bmi2 erms invpcid mpx rdseed adx smap clflushopt xsaveopt xsavec xgetbv1 xsaves dtherm ida arat pln pts hwp hwp_notify hwp_act_window hwp_epp

La frecuencia de los procesadores al ejecutar las pruebas está al minimo, es decir 400MHz

## 2. Diseño e Implementación del Software
 generator.c:
	Consta de un main que crea el archivo movie.in, primero escribe en el fichero la anchura y la altura usando la función fflush() para liberar el buffer. Después asigna espacio en el heap con la funcion malloc para los pixeles y para meta. Copia el header definido al espacio asignado a meta ("video custom binary format 0239"), luego lo escribe en el archivo movie.in y llena de 0 el espacio de meta. Copia el contenido de sfooter a meta. Finalmente escribe tantas imagenes en el fichero como esten indicadas en la variable max. Por último libera el buffer y cierra el fichero.

 video_task:
	fgauss: Esta función realiza el filtrado de gauss sobre una imagen.

	main: Escribe en el fichero de salida el ancho y alto que ha leido del fichero de entrada, inicializa los buffer de lectura y escritura, y de forma secuencial lee el fichero de entrada llama a fgauss para filtrar la imagen y la escribe en el fichero de salida, por último libera los espacios de los buffer cierra los ficheros e imprime el tiempo de ejecución en pantalla.

 video_task_paralelo: 
	fgauss: Esta función realiza el filtrado de gauss sobre una imagen.

	main: Escribe en el fichero de salida el ancho y alto que ha leido del fichero de entrada, inicializa los buffer de lectura y escritura, después comienza la región paralela, uso "#master" para que el hilo maestro sea el que crea las tareas y "task" para que después los hilos ejecuten esas tareas de forma paralela (el filtrado de la imagen llamando a fgauss), cuando "i" es igual a "seq-1" ha terminado la secuencia, por tanto utilizo "taskwait" para esperar a que terminen todas las tareas y después se escribe en movie_paralelo.out la secuencia de imágenes filtradas, una vez hecho se reinicia i a 0, esto se realiza hasta que se llegue al final del fichero. Finalmente se liberan los espacios de los buffer se cierran los ficheros y se imprime por pantalla el tiempo de ejecución.

nota: El valor de la variable "max" del generador debe ser múltiplo de "seq" para que funcione correctamente.


## 3. Metodología y desarrollo de las pruebas realizadas


Los scripts que he utilizado son los siguientes:

benchmark.sh:
#!/usr/bin/env bash
file=results4.log
touch $file
for season in 1 2; do
	for i in `seq 1 1 10`;
        do
            ./benchsuite.sh $benchcase >> $file # results dumped
            sleep 1
        done
done

benchsuite.sh:
#!/usr/bin/env bash

./build/generator
./build/video_task
./build/video_task_paralelo
diff /tmp/movie.out /tmp/movie_paralelo.out
rm /tmp/movie.in /tmp/movie.out /tmp/movie_paralelo.out

He realizado ejecuciones individuales cambiando el valor de la variable max del generador hasta obtener un tiempo de ejecución de aproximadamente 10 segundos en la versión secuencial.
Una vez he conseguido ese tiempo de ejecución secuencial he ejecutado el script benchmark.sh y he cogido los resultados que da en la segunda estación.
Los valores obtenidos de esta prueba han sido:

Para la versión secuencial:
9,567121
10,075066
9,824703
9,57605
9,566172
9,552443
9,572796
9,633728
9,564231
9,557953
Para la versión paralela:
5,103228
5,602133
5,466141
5,780512
5,048006
5,069276
5,034554
5,767324
5,713639
5,072086

El speedup obtenido al realizar la media de estos resultados es 1,7982825098.



## 4. Discusión

Esta práctica me ha parecido la más complicada de todas las de la parte de OpenMP porque me ha llevado mucho tiempo entender todo el funcionamiento del código y además la única forma que hay de identificar errores es comparando los ficheros de salida con el comando diff, por tanto tenías que revisar todo el codigo hasta encontrar cual era el fallo. El principal fallo que he tenido ha sido que no cambié el valor de la variable max del generador a un múltiplo de seq (también podría haber cambiado el valor de seq).

2. Explica brevemente la función que desarrolla cada directiva y funciones de OpenMP que has utilizado.

- omp master: El código que abarca esta directiva es solo ejecutado por el hilo maestro.
- omp task: Crea tareas explícitamente que los hilos ejecutaran de forma asíncrona.
- omp taskwait: Espera a que finalicen las tareas pendientes.

3. Etiqueta todas las variables y explica por qué les has asignado ésa etiqueta

- shared(pixels, filtered, height, width, seq)
Porque son variables que necesitan todos los hilos para el funcionamiento correcto del programa. 
- private(i,j,size)
Porque cada hilo tiene sus propias variables
Size sólo va a usarla el hilo master 

4. Explica cómo se consigue el paralelismo en éste programa 

Se consigue utilizando las tareas. El hilo maestro lee el fichero de forma secuencial y después los hilos creados en la región paralela están realizando el filtrado de gauss de forma paralela por la directiva task, las tareas se van asignando en una cola y los hilos que van quedando libres las ejecutan, cuando la secuencia de filtrado ha terminado(i==seq-1) se sincronizan las tareas con taskwait y se escriben en el fichero de salida.

5. Calcula la ganancia obtenida con la paralelización y explica los resultados obtenidos.

Al ejecutar los script de benchmark la media del tiempo de las 10 ejecuciones en secuencial ha sido 9,6490263 mientras que en la version paralela ha sido 5,3656899 por tanto el speedup obtenido en la paralelización ha sido 1,7982825098.

Esto es porque en la versión secuencial espera en cada iteración a que se realice el filtrado de la imagen, mientras que en la versión paralela solamente espera cuando se termina de filtrar una secuencia de imagenes.

